//
//  UIView+Pin.swift
//  2
//
//  Created by Anna Fedotova on 22.09.2023.
//

import UIKit

extension UIView {
    enum ConstrainedMode {
        case equal
        case grOE // greater or equal
        case lsOE // less or equal
    }
    
    // MARK: - Pin left
    @discardableResult
    func pinLeft(
        to otherView: UIView,
        _ const: Double,
        _ mode: ConstrainedMode
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, leadingAnchor, otherView.leadingAnchor, constant: const)
    }
    
    @discardableResult
    func pinLeft(
        to anchor: NSLayoutXAxisAnchor,
        _ const: Double = 0,
        _ mode: ConstrainedMode = .equal
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, leadingAnchor, anchor, constant: const)
    }
    
    // MARK: - Pin right
    
    @discardableResult
    func pinRight(
        to otherView: UIView,
        _ const: Double,
        _ mode: ConstrainedMode
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, trailingAnchor, otherView.trailingAnchor, constant: -1 * const)
    }
    
    @discardableResult
    func pinRight(
        to anchor: NSLayoutXAxisAnchor,
        _ const: Double = 0,
        _ mode: ConstrainedMode = .equal
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, trailingAnchor, anchor, constant: -1 * const)
    }
    
    // MARK: - Pin top
    
    @discardableResult
    func pinTop(
        to otherView: UIView,
        _ const: Double,
        _ mode: ConstrainedMode
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, topAnchor, otherView.topAnchor, constant: const)
    }
    
    @discardableResult
    func pinTop(
        to anchor: NSLayoutYAxisAnchor,
        _ const: Double = 0,
        _ mode: ConstrainedMode = .equal
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, topAnchor, anchor, constant: const)
    }
    
    // MARK: - Pin bottom
    
    @discardableResult
    func pinBottom(
        to otherView: UIView,
        _ const: Double,
        _ mode: ConstrainedMode
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, bottomAnchor, otherView.bottomAnchor, constant: const)
    }
    
    @discardableResult
    func pinBottom(
        to anchor: NSLayoutYAxisAnchor,
        _ const: Double = 0,
        _ mode: ConstrainedMode = .equal
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, bottomAnchor, anchor, constant: const)
    }
    
    // MARK: - Pin center
    
    func pinCenter(
        to otherView: UIView
    ) {
        pinConstraint(mode: .equal, centerXAnchor, otherView.centerXAnchor)
        pinConstraint(mode: .equal, centerYAnchor, otherView.centerYAnchor)
    }
    
    @discardableResult
    func pinXCenter(
        to otherView: UIView,
        _ const: Double = 0,
        _ mode: ConstrainedMode = .equal
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, centerXAnchor, otherView.centerXAnchor, constant: const)
    }
    
    @discardableResult
    func pinXCenter(
        to anchor: NSLayoutXAxisAnchor,
        _ const: Double = 0,
        _ mode: ConstrainedMode = .equal
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, centerXAnchor, anchor, constant: const)
    }
    
    @discardableResult
    func pinYCenter(
        to otherView: UIView,
        _ const: Double = 0,
        _ mode: ConstrainedMode = .equal
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, centerYAnchor, otherView.centerYAnchor, constant: const)
    }
    
    @discardableResult
    func pinYCenter(
        to anchor: NSLayoutYAxisAnchor,
        _ const: Double = 0,
        _ mode: ConstrainedMode = .equal
        
    ) -> NSLayoutConstraint {
        pinConstraint(mode: mode, centerYAnchor, anchor, constant: const)
    }
    
    // MARK: - Pin width
    
    @discardableResult
    func pinWidth(
        to otherView: UIView,
        _ const: Double,
        _ mode: ConstrainedMode
        
    ) -> NSLayoutConstraint {
        pinDimention(mode: mode, otherView.widthAnchor, constant: const)
    }
    
    @discardableResult
    func pinWidth(
        to dimension: NSLayoutDimension,
        _ const: Double,
        _ mode: ConstrainedMode
        
    ) -> NSLayoutConstraint {
        pinDimention(mode: mode, dimension,  constant: const)
    }
    
    // MARK: - Pin height
    
    @discardableResult
    func pinHeight(
        to otherView: UIView,
        _ const: Double,
        _ mode: ConstrainedMode
        
    ) -> NSLayoutConstraint {
        pinDimention(mode: mode, otherView.heightAnchor,  constant: const)
    }
    
    @discardableResult
    func pinHeight(
        to dimension: NSLayoutDimension,
        _ const: Double,
        _ mode: ConstrainedMode
        
    ) -> NSLayoutConstraint {
        pinDimention(mode: mode, dimension,  constant: const)
    }
    
    
    // MARK: - Private methods
    @discardableResult
    private func pinConstraint<Axis: AnyObject, AnyAnchor: NSLayoutAnchor<Axis>> (
        mode: ConstrainedMode,
        _ firstConstraint: AnyAnchor,
        _ secondConstraint: AnyAnchor,
        constant: Double = 0
    ) ->NSLayoutConstraint{
        let const = CGFloat(constant)
        let result: NSLayoutConstraint
        
        translatesAutoresizingMaskIntoConstraints = false
        
        switch mode {
        case .equal:
            result = firstConstraint.constraint(equalTo: secondConstraint, constant: const)
        case .grOE:
            result = firstConstraint.constraint(greaterThanOrEqualTo: secondConstraint, constant: const)
        case .lsOE:
            result = firstConstraint.constraint(lessThanOrEqualTo: secondConstraint, constant: const)
        }
        
        return result
    }
    @discardableResult
    private func pinConstraint(
        mode: ConstrainedMode,
        _ firstConstraint: NSLayoutDimension,
        _ secondConstraint: NSLayoutDimension,
        multiplier: Double = 0
    ) -> NSLayoutConstraint {
        let mult = CGFloat(multiplier)
        let result: NSLayoutConstraint
        
        translatesAutoresizingMaskIntoConstraints = false
        
        switch mode {
        case .equal:
            result = firstConstraint.constraint(equalTo: secondConstraint, multiplier: mult)
        case .grOE:
            result = firstConstraint.constraint(greaterThanOrEqualTo: secondConstraint, multiplier: mult)
        case .lsOE:
            result = firstConstraint.constraint(lessThanOrEqualTo: secondConstraint, multiplier: mult)
        }
        
        return result
    }
    @discardableResult
    private func pinDimention (
        mode: ConstrainedMode,
        _ dimension: NSLayoutDimension,
        constant: Double = 0
    ) ->NSLayoutConstraint{
        let const = CGFloat(constant)
        let result: NSLayoutConstraint
        
        translatesAutoresizingMaskIntoConstraints = false
        
        switch mode {
        case .equal:
            result = dimension.constraint(equalToConstant: const)
        case .grOE:
            result = dimension.constraint(greaterThanOrEqualToConstant: const)
        case .lsOE:
            result = dimension.constraint(lessThanOrEqualToConstant: const)
        }
        
        result.isActive = true
        return result
    }
}
