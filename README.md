Storyboards are slow, not readable, can cause git conflicts in real projects, it is better not to use them to gain more practical skills

25: It's telling that we want to use Auto Layout constraints to set the size and position of the label, not its frame; Not to have any conflicts with the old autoresizing mask when adding custom constraints. 29: The view is being made visible on the screen. All visible elements are contained into view hierarchies and are called subviews

Safe area layout guide gives an area of the view that will remain unobscured and allows to align views with it for maximum visibility. The area is adjusted according to the availability of top and bottom bars. It is an instance of UILayoutGuide which provides several properties to bind view with.

[weak self] is used to break strong reference cycles between the closure and self. When a closure captures self it creates a strong reference to it. If self also has a strong reference to the closure, we end up with a circular dependency, which can lead to a memory leak because the objects will never be deinitialized as each object is keeping the other one present. 

It means that any subviews of the view should be clipped to the bounds of the view. Any subviews of stack that go outside its boundaries will be clipped.

We can connect an IBAction with the event type as value changed and get a callback for the value changed. Double is a data type in Swift that represents a 64-bit floating-point number, used here to represent the value of the slider. Void is a type that represents the absence of a value. 
